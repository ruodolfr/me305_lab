"""!@file HW0X02.py
@page hw2 Modeling Motor and Paltform (2D)
@brief The following images are hand calcs performed to model the 
       motion of a motor and platform in 2D.
@n

@image html HW0x021.jpg width=1279px height=1639px
@image html HW0x022.jpg width=1279px height=1639px
@image html HW0x023.jpg width=1279px height=1639px
@image html HW0x024.jpg width=1279px height=1639px
"""

