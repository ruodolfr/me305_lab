"""!@file HW0X03.py
@page hw3 Simulation of 2D Ball and Platform Model
@details    After HW2 a solution of the kinematic and kinectics of the ball 
            and platform system were given along with system parameters. Using
            this information, a simulation of the system was conducted using 
            MATLAB. 
            
    
@image html ME305HW3RUMBAOA-1.jpg width=879px height=1239px
@image html ME305HW3RUMBAOA-2.jpg width=879px height=1239px
@image html ME305HW3RUMBAOA-3.jpg width=879px height=1239px
@image html ME305HW3RUMBAOA-4.jpg width=879px height=1239px
@image html ME305HW3RUMBAOA-5.jpg width=879px height=1239px
@n

@author Ruodolf Rumbaoa
@date 2/21/2022
"""