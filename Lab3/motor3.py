"""!@file motor3.py
@brief Motor driver file used to create motor objects for lab 3.
"""
from pyb import Pin, Timer

class Motor:
     '''!@brief A motor class for one channel of the DRV8847.
     @details Objects of this class can be used to apply PWM to a given
     DC motor.
     '''
     def __init__ (self, PWM_tim, timch1, timch2, IN1_pin, IN2_pin):
         '''!@brief Initializes and returns an object associated with a DC Motor.
         @details Objects of this class should not be instantiated
         directly. Instead create a DRV8847 object and use
         that to create Motor objects using the method
         DRV8847.motor().
         @param PWM_tim Chooses the timer being used for PWM
         @param timch1 Sets channel 1 for timer
         @param timch2 Sets channel 2 for timer
         @param IN1_pin Sets input pin for channel 1
         @param IN2_pin Sets input pin for channel 2

         '''

         self.t3ch1 = PWM_tim.channel(timch1, Timer.PWM_INVERTED, pin=IN1_pin)
         self.t3ch2 = PWM_tim.channel(timch2, Timer.PWM_INVERTED, pin=IN2_pin)
         
         pass
    
     def set_duty (self, duty):
         '''!@brief Set the PWM duty cycle for the motor channel.
         @details This method sets the duty cycle to be sent
         to the motor to the given level. Positive values
         cause effort in one direction, negative values
         in the opposite direction.
         @param duty A signed number holding the duty
         cycle of the PWM signal sent to the motor
         '''
         #Forwards Motion
         if duty >= 0:
         
             self.t3ch1.pulse_width_percent(0)
             self.t3ch2.pulse_width_percent(duty)
             
         elif duty < 0:
             self.t3ch1.pulse_width_percent(-1* duty)
             self.t3ch2.pulse_width_percent(0)
         pass
    








if __name__ == '__main__':
 # Adjust the following code to write a test program for your motor class. Any
 # code within the if __name__ == '__main__' block will only run when the
 # script is executed as a standalone program. If the script is imported as
 # a module the code block will not run.

 # Create a timer object to use for motor control
    PWM_tim = Timer(3, freq = 20_000)
    
  



    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_1 = Motor(PWM_tim, 1, 2, Pin.cpu.B4, Pin.cpu.B5)
    motor_2 = Motor(PWM_tim, 3, 4, Pin.cpu.B0, Pin.cpu.B1)
    # Enable the motor driver
    nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
    nSLEEP.high()
    
    # Set the duty cycle of the first motor to 40 percent
    motor_1.set_duty(70)
    motor_2.set_duty(20)