"""!@file taskEncoder2.py

@brief Program that interfaces the UI with the encoder driver.
@n
@details    This program is a FSM that takes in the flags raised by the UI, which 
            represent keyboard inputs by the user. It creates an instance of the
            encoder class (encoder.py), and then calls methods from the object 
            based on the flags. It passes the outputs of the methods back to the
            UI as needed.
    
@author Theo Philliber
@author Ruodolf Rumbaoa
@date 1/20/2022
"""
from time import ticks_us, ticks_add, ticks_diff
import pyb, array
import encoder2, micropython

S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POS = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_RECORD = micropython.const(5)
S6_CSV = micropython.const(6)
S7_VELO = micropython.const(7)
S8_NUMIN = micropython.const(8)

def taskEncFcn(taskName, period, zFlag, pFlag, dFlag, gFlag, sFlag, vFlag, mFlag, MFlag, dataList, timeList, printShare, recDone):
    '''!@brief              FSM code that runs the encoder task.
       @details            The encoder task reads flags raised by the user task and performs the 
                           corresponding function.
       @param taskName     The name of the task as a string.
       @param period       The task interval or period specified as an integer
                           number of microseconds.
       @param zFlag        Boolean variable (shares class) representing if the
                           'z' key has been pressed.
       @param pFlag        Boolean variable (shares class) representing if the
                           'p' key has been pressed.
       @param dFlag        Boolean variable (shares class) representing if the
                           'd' key has been pressed.
       @param gFlag        Boolean variable (shares class) representing if the
                           'g' key has been pressed.
       @param sFlag        Boolean variable (shares class) representing if the
                           's' key has been pressed.
                           
       @param vFlag        Boolean variable (shares class) representing if the
                           'v' key has been pressed.
       @param mFlag        Boolean variable (shares class) representing if the
                           'm' key has been pressed.
       @param MFlag        Boolean variable (shares class) representing if the
                           'M' key has been pressed.
       @param dataList     Shares class used for printing comma separated list
                           of collected data. 
       @param printShare   Shares class used for storing and printing data from
                           position and delta functions called by taskEncoder.py.
       @param recDone      Boolean (shares class) telling whether recording has
                           finished or not.
   '''
    state = S0_INIT
    
    myEncoder1 = encoder2.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    current_time = ticks_us()
    
    state = S1_WAIT
    while True:
        # Find current time
       
        current_time = ticks_us()
        
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
    
                
            if state == S1_WAIT:
                myEncoder1.update()
                
                if zFlag.read():
                     #print('Encoder zeroing')
                     myEncoder1.zero(myEncoder1.position)
                     zFlag.write(False)
                     state = S1_WAIT
                     
                elif pFlag.read():
                    #print('Getting encoder position...')
                    printShare.write(myEncoder1.get_position())
                    pFlag.write(False)
                    state = S1_WAIT
                    
                elif dFlag.read():
                    #print('Getting delta of encoder reading...')
                    printShare.write(myEncoder1.get_delta())
                    dFlag.write(False)
                    state = S1_WAIT
                
                elif gFlag.read():
                    print('Recording data for 30 seconds. Press the s key to stop recording early. Output will be in a .CSV file')
                    record_start_time = ticks_us()

                    #timeArray = array.array('l', 3001*[0])
                    #posArray = array.array('l', 3001*[0])
                    #timeArray = array.array('l',[0])
                    #posArray = array.array('l',[0])

                    i = 0
                    state = S5_RECORD
                    
                elif vFlag.read():
                     #print('Encoder zeroing')
                     
                     printShare.write((myEncoder1.get_delta())*(2*3.141592/4000)/((ticks_diff(current_time, previous_time)/1_000_000)))
                     vFlag.write(False)
                     state = S1_WAIT
                    
            elif state == S5_RECORD:
                
               
                
                if (sFlag.read()) or (current_time > record_start_time + 30_000_000):
                    #dataList.write([timeArray[0:i], posArray[0:i]])
                    
                    recDone.write(True)
                    sFlag.write(False)
                    gFlag.write(False)
                    state = S6_CSV
                
                else:
                    #i <= (len(timeArray) - 1):
                    myEncoder1.update()
                    dataList.put(myEncoder1.get_position())
                    timeList.put(float("{:.2f}".format((current_time - record_start_time)/1_000_000)))
                    #timeArray[i], posArray[i] = (current_time - record_start_time, myEncoder1.get_position())
                    #timeArray.append(current_time - record_start_time)
                    #posArray.append(myEncoder1.get_position())

                                      
                    #i += 1
                    
            elif state == S6_CSV:
                #if not recDone.read():
                if dataList.num_in() <= 0:
                    state = S1_WAIT
                else:
                    myEncoder1.update()
                
                
                
               # else:                    
                    #myEncoder1.update()
                    #data_Array[i] = myEncoder1.get_position();
                    #if i % 10 == 0: 
                     #   data_Array.append(myEncoder1.get_position())
                    #i += 1
                                
            
            
            previous_time = current_time
            yield state
        else:
            yield None