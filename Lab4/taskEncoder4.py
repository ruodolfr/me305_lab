"""!@file taskEncoder4.py

@brief Program that interfaces the UI with the encoder driver.
@n
@details    This program is a FSM that takes in the flags raised by the UI, which 
            represent keyboard inputs by the user. It creates an instance of the
            encoder class (encoder.py), and then calls methods from the object 
            based on the flags. It passes the outputs of the methods back to the
            UI as needed.
    
@author Theo Philliber
@author Ruodolf Rumbaoa
@date 2/17/2022
"""
from time import ticks_us, ticks_add, ticks_diff
import pyb, array
import encoder4, micropython

S0_INIT = micropython.const(0)
S1_WAIT = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POS = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_RECORD = micropython.const(5)
S6_CSV = micropython.const(6)
S7_VELO = micropython.const(7)
S8_NUMIN = micropython.const(8)
S17_STEP_TEST = micropython.const(17)


def taskEncFcn(taskName, period, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC):
    '''!@brief         FSM code that runs the motor task.
        @details         The motor task reads flags raised by the user task and encoder task 
                       and performs the corresponding function.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                       number of microseconds.
                       
        @param keyFlag      Boolean variable (shares class) representing user key input
        @param taskFin      Share used as a flag raised when moving between states
        @param velShare     Share used for stroing motor velocity data
        @param posShare     Shares class used for storing motor shaft position data
                            of collected data. 
        @param valueShare   Stores duty cycle value to apply duty to motor 1 or motor 1
        @param timeShare    Stores time data used for calculating velocity
        @param CLC          Boolean variable that allows user to enable/disable closed
                            loop motor control
    ''' 
    state = S0_INIT
    
    myEncoder1 = encoder4.Encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    #Set initial aribitrary value for previous time on first run
    previous_time = ticks_us()
    current_time = ticks_us()
  
    
    state = S1_WAIT
    while True:
        # Find current time
       
        current_time = ticks_us()
        
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            myEncoder1.update()
            velShare.write(myEncoder1.get_velocity(ticks_diff(current_time, previous_time)))
                
            if state == S1_WAIT:
               
                
                # Check if keyFlag contains a value
                if keyFlag.read():
                    if keyFlag.read() == 'z':
                         myEncoder1.zero(myEncoder1.position)
                         keyFlag.write(None)
                         taskFin.write(True)
                         state = S1_WAIT
                         
                    elif keyFlag.read() == 'p':
                        
                        posShare.write(myEncoder1.get_position())
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                        
                    elif keyFlag.read() == 'd':
                        valueShare.write(myEncoder1.get_delta())
                        keyFlag.write(None)
                        taskFin.write(True)
                        state = S1_WAIT
                    
                    elif keyFlag.read() == 'g':
                        record_start_time = ticks_us()
                        keyFlag.write(None)
                        timeList = None
                        #velList = None
                        posList = None
                        #timeList = array.array('l', 3001*[0])
                        i = 0
                        state = S5_RECORD
                        
                    elif keyFlag.read() == 'v':
                         #print('Encoder zeroing')
                         
                         #velShare.write(myEncoder1.get_velocity(ticks_diff(current_time, previous_time)))
                         #printShare.write((myEncoder1.get_delta())*(2*3.141592/4000)/((ticks_diff(current_time, previous_time)/1_000_000)))
                         valueShare.write(velShare.read())
                         keyFlag.write(None)
                         taskFin.write(True)
                         state = S1_WAIT
                     
                    elif keyFlag.read() == 'r':
                        if valueShare.read():
                            timeList    = array.array('l', 305*[0])
                            velList     = array.array('f', 305*[0])
                            i = 0
                            stepTimeStart = current_time
                            state = S17_STEP_TEST
                    
            
            elif state == S17_STEP_TEST:
                if not taskFin.read():
                    velList[i] = velShare.read()
                    timeList[i] = ticks_diff(current_time, stepTimeStart)
                    i += 1
                elif (velList != None) and (valueShare.read() == None):
                    valueShare.write(velList)
                    velList = None
                elif (timeList != None) and (valueShare.read() == None):
                    valueShare.write(timeList)
                    timeList = None
                    state = S1_WAIT
                    
            
            elif state == S5_RECORD:
                
                if timeList == None:
                    timeList = array.array('l', 3001*[0])
                elif posList == None:
                    posList = array.array('I', 3001*[0])
               
                elif (keyFlag.read()) or (current_time > record_start_time + 30_000_000):
                    taskFin.write(True)
                    keyFlag.write(None)
                    k = 0
                    valueShare.write(None)
                    state = S6_CSV
                
                else:
                    posList[i] = myEncoder1.get_position()
                    timeList[i] = current_time - record_start_time
                    i += 1
                         
               
                    
            elif state == S6_CSV:
                if k < i:
                    if valueShare.read() == None:
                        #valueShare.write((timeList.pop([0]), posList.pop([0])))
                        valueShare.write(((timeList[k]), posList[k]))
                        k+= 1
                else:
                    taskFin.write(True)
                    i = 0
                    k = 0
                    state = S1_WAIT
            
                
            
            
            
            previous_time = current_time
            yield state
        else:
            yield None