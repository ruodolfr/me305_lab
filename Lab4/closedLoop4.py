"""!@file closedLoop4.py
@brief  This file is the driver used to update duty cycles applied to 
        the motor and update the closed loop proportional gain.
        
@author Theo Philiber
@author Ruodolf Rumbaoa
@date   02/17/2022
"""

class ClosedLoop:
    '''!@brief 
        @details
    '''
        
    def __init__(self, Kp, Ki):
        '''!@brief Constructs a Closed-Loop Controller object
            @details
        '''
        self.Kp = Kp
        self.Ki = Ki
        pass
    
    def update(self, v_ref, v_meas):
        
        #Proportional Contribution
        duty_adjusted = self.Kp*(v_ref - v_meas)
        
        #Integral contribution
        
        if duty_adjusted > 100:
            duty_adjusted = 100
        elif duty_adjusted < -100:
            duty_adjusted = -100
        return duty_adjusted
    
    def set_gain(self, gain, gain_type):
        ## Gain_type: Kp = 1, Ki = 2;
        if gain_type == 1:
            self.Kp = gain
        elif gain_type == 2:
            self.Ki = gain
        else:
            pass
    