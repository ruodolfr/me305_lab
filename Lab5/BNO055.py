'''!@file BNO055.py
@brief An IMU driver class to make BNO055 IMU objects
'''
from pyb import I2C
import time

class BNO055:
    
    
    
    def __init__(self, i2c):
        '''@brief Create IMU driver with I2C in controller mode'''
        self.i2c = i2c
        
        #Set euler angle readings to radians:
        self.i2c.mem_write(0b00000100, 0x28, 0x3B)
        
        
        
        pass
        
    def config_mode(self):
        self.i2c.mem_write(0b00000000, 0x28, 0x3D)
        pass
    
    
    def IMU_mode(self):
        '''@brief Configure driver in IMU fusion mode'''
        self.i2c.mem_write(0b00001000, 0x28, 0x3D)
        pass
        
        
    def calib_status(self):
        '''@brief Read calibration status byte from IMU, parse into individual statuses
           @return self.acc_stat Returns the calibration status of the accelerometer as 0 for not calibrated, 
                                 1 for partially calibrated, and 3 for fully calibrated
           @return self.gyr_stat Returns the calibration status of the gyroscope as 0 for not calibrated,
                                 1 for partiallycalibrated, and 3 for fully calibrated
        '''
        
        cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
        
        #print(cal_byte)
        
        self.mag_stat    =  cal_byte & 0b00000011
        self.acc_stat    = (cal_byte & 0b00001100) >> 2
        self.gyr_stat    = (cal_byte & 0b00110000) >> 4
        self.sys_stat    = (cal_byte & 0b11000000) >> 6
        
        #print(f'Mag: {mag_stat}, Acc: {acc_stat}, Gyr: {gyr_stat}, Sys: {sys_stat}')
        
        return (self.acc_stat, self.gyr_stat)
        
    def calib_read(self):
        '''@brief Read calibration coefficients as array of packed binary data
           @return calib_array Returns a bytes obtject of the IMU calibration constants 
        '''
        
        #Set to CONFIG_MODE
        self.config_mode()
        #Read calibration coefficient registers
        calib_bytearray = self.i2c.mem_read(22, 0x28, 0x55)
        
        
        print(calib_bytearray)
        
        
        
        #Return to IMU mode
        self.IMU_mode()
        return calib_bytearray
        
    def calib_write(self, offset_array):
        '''@brief Write calibration coefficients to IMU registers from known binary data array
           @param offset_array A bytes object read from a text file of IMU offset values from a 
                               previous calibration procedure
        '''
        
        self.config_mode()
        self.i2c.mem_write(offset_array, 0x28, 0x55)
        
        
        
        
        
        
        #Return to IMU mode
        self.IMU_mode()
        
        pass
        
        
    def euler_angle(self):
        '''@brief Read Euler angles from IMU for feedback measurements'''
        
        (rollMsb, rollLsb, pitchMsb, pitchLsb) = tuple(self.i2c.mem_read(4, 0x28, 0x1C))
        #pitchArray = self.i2c.mem_read(4, 0x28, 0x1E) 
        self.roll = (rollMsb+rollLsb)/900
        self.pitch = (pitchMsb+pitchLsb)/900
        
        #print(roll, pitch)
        
        return None
        
        
        
        
        
    def angular_velo(self):
        '''@brief Read angular velocities from IMU for feedback measurements'''
    
        (rollvMsb, rollvLsb, pitchvMsb, pitchvLsb) = tuple(self.i2c.mem_read(4, 0x28, 0x14))
        #pitchArray = self.i2c.mem_read(4, 0x28, 0x1E) 
        rollv = (rollvMsb+rollvLsb)/900
        pitchv = (pitchvMsb+pitchvLsb)/900
        
        print(rollv, pitchv)
        
        pass
    
    
    
    
    
    
    
    
    
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
   
    myI2C = I2C(1, I2C.CONTROLLER)
    offset_array = b'\xf3\xff\xfa\xff\xe5\xff\x00\x10\x10\x01\x04\x00\xff\xff\xff\xff\x00\x00\xe8\x03\xe0\x01'
    driver1 = BNO055(myI2C)
    driver1.IMU_mode()
    driver1.calib_status()
    driver1.calib_write(offset_array)
    while True:
        driver1.angular_velo()
        driver1.calib_read()
        driver1.calib_status()
        time.sleep(3)
        #print(driver1.i2c.mem_read(1, 0x28, 0x00))
    
    