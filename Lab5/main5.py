"""!@file main5.py
@brief Main file for lab 5. Added functionality of IMU communication.
@n
@page lab5  Lab 0x05: I2C and IMU
@brief      Added functionality of IMU communication and IMU data reading.
@details    This code reads input from a 9 DOF IMU (BNO055). There is a 
            user interface (taskUser.py) which allows the user to control the 
            IMU to perform several functions. It also prints out a description
            of what the functions are and how to execute them. The file taskBNO055.py
            acts as an interface between the UI file and the IMU driver (BNO055.py),
            calling methods from the IMU and passing them to the UI to print.
            The driver file sets up an IMU class and creates the methods used
            by the other files.
@n
@image html taskMotorFSM.png
@image html taskUserFSM.png
@image html taskIMUFSM.png
          

@n     
Source Folder
    [https://bitbucket.org/ruodolfr/me305_lab/src/master/Lab5/]
@n

@author Theo Philliber
@author Ruodolf Rumbaoa
@date 02/24/2022

"""
import taskUser5, taskBNO055, shares, taskMotor5


if __name__ == '__main__':
    
    # Share definitions:
    # String shares:
    keyFlag = shares.Share(None)
    
    # Boolean shares:
    taskFin = shares.Share(False)
    CLC = shares.Share(False)
    
    # Numerical shares:
    valueShare = shares.Share(None)
    velShare = shares.Share(0)
    posShare = shares.Share(0)
    timeShare = shares.Share(0)
    
    rollShare = shares.Share(0)
    pitchShare = shares.Share(0)
            
        
   
    
    
    #offset_array = b'\xf3\xff\xfa\xff\xe5\xff\x00\x10\x10\x01\x04\x00\xff\xff\xff\xff\x00\x00\xe8\x03\xe0\x01'
    
    
# =============================================================================
#     myIMU.calib_status()
#     #myIMU.calib_write(offset_array)
#     while True:
#         myIMU.angular_velo()
#         myIMU.calib_read()
#         myIMU.calib_status()
# =============================================================================
        
    
    
    
    # Run tasks one by one
    taskList = [taskUser5.taskUserFcn('Task User', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare), 
                taskBNO055.taskIMUFcn('Task IMU', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare),
                taskMotor5.taskMotorFnc('Task Motor', 10_000, keyFlag, taskFin, valueShare, velShare, posShare, timeShare, CLC, rollShare, pitchShare)]

    
    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

