'''!@file touchPanelDriver.py
    @brief Touch Panel driver class.
    @details Creates class for touch panel, with methods to initialize,
    calibrate, and read data.
'''
from pyb import Pin, ADC
from micropython import const
import time
from ulab import numpy as np


class TouchPanel:
    
    def __init__(self):
        '''!@brief Create Touch Panel driver and configure pins
            @details Configures pins for x(+/-) and y(+/-). Establishes constants
            for conversion from ADC voltage to mm.            
        '''
        
        
        #Create Positive (p) and negative (m) input pins for x and y directions
        self.ym = Pin.cpu.A0
        self.xm = Pin.cpu.A1
        self.yp = Pin.cpu.A6
        self.xp = Pin.cpu.A7
       
        self.bit12      = const(4096)
        self.xscale     = const(176/self.bit12)
        self.xoff       = const(88)
        self.yscale     = const(100/self.bit12)
        self.yoff       = const(50)
        self.settle     = const(3.6/1_000_000)

        self.Kxx      = const(1)
        self.Kxy      = const(0)
        self.Kyx      = const(0)
        self.Kyy      = const(1)
        self.Xoff     = const(0)
        self.Yoff     = const(0)
        
        pass
    
    def XYZ(self):
        '''!@brief Scans data from x, y, and contact.
            @details Configures pins to scan from x, z, then y axes. This order
            (XZY) has fewer pin reconfigurations than XYZ. After obtaining raw data,
            the data is converted into position in mm. 
            @return Tuple with xpos, ypos, and contact.            
        '''
    #X Configures for and calculates position along x-direction
        self.xm.init(Pin.OUT_PP, value=0)
        self.xp.init(Pin.OUT_PP, value=1)
        self.ym.init(Pin.IN)
        self.yp.init(Pin.ANALOG)
        self.ADC = ADC(self.yp)
        
        time.sleep(self.settle)
        self.xraw = (self.ADC.read()*self.xscale - self.xoff)
    
    
    #Z measurement (second because fewer pin reconfigures)
        self.xp.init(Pin.ANALOG)
        self.yp.init(Pin.OUT_PP, value=1)
        self.ADC = ADC(self.xp)
        
        if self.ADC.read() > 50:
            self.contact = 1
        else:
            self.contact = 0

    #Y
        self.ym.init(Pin.OUT_PP, value=0)       
        self.xm.init(Pin.IN)
        time.sleep(self.settle)

        self.yraw = self.ADC.read()*self.yscale - self.yoff
        self.xpos = self.xraw*self.Kxx + self.yraw*self.Kxy + self.Xoff
        self.ypos = self.yraw*self.Kyy + self.xraw*self.Kyx + self.Yoff
        
        return(self.xpos, self.ypos, self.contact)
    
    def CalibWrite(self, BMAT):
        '''!@brief Changes calibration values used in XYZ() conversion.
            @details Takes calibration values calculated in taskTouchPanel.py.
            On subsequent runs of XYZ(), the conversions will incorporate these
            values to improve the accuracy of the panel.
            @param BMAT 3x2 Matrix holding calibration constants.
        '''
            
        self.Kxx      = const(BMAT[0,0])
        self.Kyx      = const(BMAT[0,1])
        self.Kxy      = const(BMAT[1,0])
        self.Kyy      = const(BMAT[1,1])
        self.Xoff     = const(BMAT[2,0])
        self.Yoff     = const(BMAT[2,1])
        pass

        
        
if __name__ == '__main__':

    myTP = TouchPanel()
    myTP.setX()
   
    while True:
        try:           
            print(myTP.XYZ())
            #if myTP.contact == True:
                #print(myTP.xpos, myTP.ypos)
            
            
        except KeyboardInterrupt:
        
            break

    
    pass
