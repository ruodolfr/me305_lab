'''!@file       taskTouchPanel.py
    @brief      Task function file for touch panel.
    @details    This task file creates a function that will be run in cooperative
    multitasking mode. Runs touch panel driver to get position values, and inserts
    data into shares. Also calculates the difference between data values to estimate
    velocity. Finally, the function has a calibration protocol, allowing for the user
    to tap the corners of the panel and improve its accuracy.
    @author     Theo Philliber
    @author     Ruodolf Rumbaoa
    @date       03/18/2022
'''

from time import ticks_us, ticks_add, ticks_diff
import micropython, pyb
from touchPanelDriver import TouchPanel
from ulab import numpy as np

#   S0_INIT
#   Initializes taskTouchPanelFcn function. Creates touch panel object.
S0_INIT = micropython.const(0)
#   Reads data from the touch panel. Uses the difference between points
#   to calculate the velocity in each direction. Writes all data to shares.
S1_READ = micropython.const(1)
#S2_FILTER
#Filters data from touch panel to reduce noise. NOT CURRENTLY IMPLEMENTED
S2_FILTER = micropython.const(2)
#S3_CALIBRATE
#Calibration sequence for touch panel. Prompts user to touch designated
#   corners of panel, then center. Computes the constants, which are then used in
#   S1_READ to improve the touch panel's accuracy.
S3_CALIBRATE = micropython.const(3)


def taskTouchPanelFcn(taskName, period, KEYFLAG, TASKFIN, BALLPOS, BALLVEL):


    '''!@brief              Generator function to run the touch panel task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified. Runs touch panel 
                            driver to get position values, and inserts data into shares.
                            Also calculates the difference between data values to estimate
                            velocity. Finally, the function has a calibration protocol, allowing
                            for the user to tap the corners of the panel and improve its 
                            accuracy.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param KEYFLAG      Flag containing value of user inputted key command
        @param TASKFIN      Share telling if task has finished across task files,
                            similar to acknowledge bit in I2C communication.
        @param BALLPOS Share containing tuple of ball position (x, y, contact).
        x and y are in mm; contact == 1 if ball is present, 0 if not.
        @param BALLVEL Share containing tuple of ball velocity (xdot, ydot).
        
                            
                            
    '''
    
    # Start at State 0, instance touch panel object
    state = S0_INIT
    
    myTP = TouchPanel()
    YMAT = np.array(((80,40), (80,-40), (-80,40), (-80,-40), (0,0)))
    XMAT = np.array(((0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0)))
    BMAT = np.array(((0,0), (0,0), (0,0)))
    
    # Transition to State 1: Wait for input. 
    state = S1_READ
   
    # Establish start time to control rate at which code runs.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    previous_time = ticks_us()
    
    while True:
        # Find current time
        current_time = ticks_us()
        # Check if time period has passed since last time code ran.
        if ticks_diff(current_time, next_time) >= 0:
            # Update next_time value for next code iteration.
            next_time = ticks_add(next_time, period)
            
            if state == S1_READ:
                
                
                (x_prev, y_prev) = BALLPOS.read()[0:2]
                
                BALLPOS.write(myTP.XYZ())
                
                
                x_dot = (BALLPOS.read()[0] - x_prev)*1_000_000/ticks_diff(current_time, previous_time)
                y_dot = (BALLPOS.read()[1] - y_prev)*1_000_000/ticks_diff(current_time, previous_time)
                
                BALLVEL.write((x_dot, y_dot))
                
                
                
                
                if KEYFLAG.read() == 'c':
                    
                    print('   A____________ ____________B    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |____________|____________|    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |____________E____________|    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |            |            |    ')
                    print('   |____________|____________|  X ')
                    print('   |            |            |  ^ ')
                    print('   |            |            |  | ')
                    print('   |            |            |  | ')
                    print('   C____________|____________D  | ')
                    print('                                | ')
                    print('                     Y <--------+ ')
                    print("")
                    print('Touch the locations sequentially from A to E.')
                    print('This will calibrate the corner and center    ')
                    print('locations of the touchpad.                   ')
                    print("")


                    #Number index of corner for calibration
                    cornCount = 0
                    toggle = False
                    cornArray = "ABCDE"
                    KEYFLAG.write(None)
                    print("The following locations are in units of [mm]:")
                    print("")
                    state = S3_CALIBRATE
                    
                    
                    
                    
            elif state == S2_FILTER:
                
                pass
            
            elif state == S3_CALIBRATE:
                (x, y, contact) = myTP.XYZ()
                
                if cornCount >= 5:
                    state = S1_READ
                    TASKFIN.write(True)
                    XTRANS = np.array(XMAT).transpose()
                    XINV = np.linalg.inv(np.dot(XTRANS, XMAT))
                    BMAT = np.dot(np.dot(XINV, XTRANS), YMAT)
                    #corrected corner locations:
                    print("")
                    print('Corrected locations [mm]:')
                    print("")
                    print(f'A:({np.dot(XMAT, BMAT)[0,0]},{np.dot(XMAT, BMAT)[0,1]})')
                    print(f'B:({np.dot(XMAT, BMAT)[1,0]},{np.dot(XMAT, BMAT)[1,1]})')
                    print(f'C:({np.dot(XMAT, BMAT)[2,0]},{np.dot(XMAT, BMAT)[2,1]})')
                    print(f'D:({np.dot(XMAT, BMAT)[3,0]},{np.dot(XMAT, BMAT)[3,1]})')
                    print(f'E:({np.dot(XMAT, BMAT)[4,0]},{np.dot(XMAT, BMAT)[4,1]})')
                    print("")

                    #print((BMAT[1,1]))
                    myTP.CalibWrite(BMAT)
                    state = S1_READ
                if (contact == 1) and (toggle == False):
                    toggle = True
                    
                    XMAT[cornCount] = (x, y, 1)
                    
                    print(f"{cornArray[cornCount]}: {x}, {y}")
                    
                    
                elif (contact == 0) and (toggle == True):
                    cornCount += 1
                    toggle = False

            previous_time = current_time 
            yield state
    else:
        
        yield None
          
            
                