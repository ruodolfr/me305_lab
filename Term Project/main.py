"""!@file main.py
@page TP Term Project: Ball Balancing
@brief Main file for the Term Project.
@details This file runs the Motor, UI, and IMU tasks sequentially to balance a
ball on a platform actuaded by two motors.
@n

System Overview Video: [https://youtu.be/G-cDOQUt1_0]
@n
\htmlonly
<iframe src="https://player.vimeo.com/video/689911360?h=079c796bb4&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="System overview"></iframe>\endhtmlonly
@n
Task Diagram
@n
@image html systemTaskDiagram.png
@n
FSM Diagrams For Motor, IMU, UI, and Touch Panel Tasks            
@image html taskMotorFSM.png
@image html taskUserFSM.png
@image html taskIMUFSM.png
@image html taskTouchPanelFSM.png
@n
Putty Outputs
@n
@image html initialui.png
@image html tpcalib.png
@image html settinginnerandoutergains.png
         
Source Folder
    [https://bitbucket.org/ruodolfr/me305_lab/src/master/]
@n
Playlist of Tuning Tirals
    [https://www.youtube.com/playlist?list=PLZd4ce8wMcw1sTvo_lK91g9U7heqjyGdl]
@n
@author Theo Philliber
@author Ruodolf Rumbaoa
@date 03/11/2022

"""
import taskUser, taskIMU, taskTouchPanel, taskMotor, shares
#taskDataCollect, taskMotor, shares
import taskUser, taskTouchPanel, shares
from micropython import const

if __name__ == '__main__':
    
    period          = const(10000)
    #periodTP (touch panel) for running taskTouchPanel at different rate, allowing for multiple data samples per iteration
    periodTP        = const(10000)
    
    
    
    # Share definitions:
    # String shares:
    KEYFLAG         = shares.Share(None)
    
    # Boolean shares:
    TASKFIN         = shares.Share(False)
    ENABLE          = shares.Share(False)
    
    # Numerical shares:
    BALLPOS         = shares.Share((0,0,0))
    BALLVEL         = shares.Share((0,0))
    KPSHARE         = shares.Share(None)
    KDSHARE         = shares.Share(None)
    
    
    
    #valueShare = shares.Share(None)
    velShare = shares.Share(0)
    posShare = shares.Share(0)
    timeShare = shares.Share(0)
    
    rollShare = shares.Share(0)
    pitchShare = shares.Share(0)
    rollvShare = shares.Share(0)
    pitchvShare = shares.Share(0)
    
    # Run tasks one by one
    taskList = [taskUser.taskUserFcn('Task User', period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE),
                taskIMU.taskIMUFcn('Task IMU', period, TASKFIN, rollShare, pitchShare, rollvShare, pitchvShare),
                taskTouchPanel.taskTouchPanelFcn('Task Touch Panel', periodTP, KEYFLAG, TASKFIN, BALLPOS, BALLVEL),
                #taskDataCollect.taskDataCollectFcn('Task Data', period,),
                taskMotor.taskMotorFcn('Task Motor', period, ENABLE, KEYFLAG, TASKFIN, KPSHARE, KDSHARE, BALLPOS, BALLVEL, rollShare, pitchShare, rollvShare, pitchvShare)]

    
    
    while True:
        
        try:
            for task in taskList:
                
              
                next(task)
           
        
        except KeyboardInterrupt:
        
            break
    print('Program Terminating')

