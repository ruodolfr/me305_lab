'''!@file BNO055.py
    @brief Driver file for 9-axis IMU.
    @details This file contains code to operate Bosch's BNO055 Inertial Measurement
    Unit. It runs the accelerometer, gyroscope, and magnetometer integrated in
    the MEMS unit. The code creates a class [BNO055] which will be instanced by
    taskIMU, allowing it to configure, read, and write data to and from the 
    sensor.
    @author Theo Philliber
    @author Ruodolf Rumbaoa
    @date 3/18/2022
'''
from pyb import I2C
import time, struct

class BNO055:
    
    
    
    def __init__(self, i2c):
        '''!@brief Create IMU driver with I2C in controller mode
            @details This function is called upon initialization of BNO055 
            class. The I2C object will be passed into __init__, allowing for
            communication between the MCU and IMU sensor.
            @param i2c - I2C object, found in pyb library.
        '''
        self.i2c = i2c
        
        #Set euler angle readings to radians:
        self.i2c.mem_write(0b00000100, 0x28, 0x3B)
        
        
        
        pass
        
    def config_mode(self):
        '''!@brief Sets IMU to configuration mode.
            @details In configuration mode, the IMU can be read calibrated. This
            function is used by the functions calib_status and calib_read, to 
            read and set the calibration constants stored in the IMU registers.
        '''
        self.i2c.mem_write(0b00000000, 0x28, 0x3D)
        pass
    
    
    def IMU_mode(self):
        '''!@brief Sets IMU sensor to IMU fusion mode.
            @details In IMU mode, the IMU will continuously compute data from 
            each axis, which will be outputted and read by the MCU.
        '''
        self.i2c.mem_write(0b00001000, 0x28, 0x3D)
        pass
        
        
    def calib_status(self):
        '''!@brief Read calibration status byte from IMU, parse into individual statuses
            @details Reads the onboard calibration statuses from IMU registers.
            These have a value ranging from 0 (non calibrated) to 3 (fully
            calibrated). 
        '''
        
        cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
        
        self.mag_stat    =  cal_byte & 0b00000011
        self.acc_stat    = (cal_byte & 0b00001100) >> 2
        self.gyr_stat    = (cal_byte & 0b00110000) >> 4
        self.sys_stat    = (cal_byte & 0b11000000) >> 6
        
        
        
        return (self.acc_stat, self.gyr_stat)
        
    def calib_read(self):
        '''!@brief Read calibration coefficients as array of packed binary data
            @return Returns calibration coefficients as 22-byte-long bytearray.
        '''
        
        #Set to CONFIG_MODE
        self.config_mode()
        
        #Read calibration coefficient registers
        buf = bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
        
        calib_bytearray = self.i2c.mem_read(buf, 0x28, 0x55)
        
        #Return to IMU mode
        self.IMU_mode()
        return calib_bytearray
        
    def calib_write(self, offset_array):
        '''!@brief Write calibration coefficients to IMU registers from known binary data array
            @param offset_array 22-byte-long bytearray containing calibration coefficients.
        '''
        
        self.config_mode()   
        self.i2c.mem_write(offset_array, 0x28, 0x55)
        
        #Return to IMU mode
        self.IMU_mode()
        
        pass
        
        
    def euler_angle(self):
        '''!@brief Read Euler angles from IMU for feedback measurements
            @details Reads the data from IMU Euler angle registers for roll and
            pitch (heading not needed). Data is 4 bytes, and stored in bytearray
            'buf'. Data is unpacked into two 16-bit integers, and converted to 
            radians.
        '''
        
        
        buf = bytearray([0,0,0,0])
        self.i2c.mem_read(buf, 0x28, 0x1C)
        xraw, yraw = (struct.unpack('<hh', buf))
        self.roll = xraw/900
        self.pitch = yraw/900
        
        
        return None
        
        
        
        
    def angular_velo(self):
        '''!@brief Read angular velocities from IMU for feedback measurements
            @details Reads the data from IMU Euler angle velocity registers for 
            roll and pitch velocity (heading not needed). Data is 4 bytes, and 
            stored in bytearray 'buf'. Data is unpacked into two 16-bit integers,
            and converted to radians.
        '''
    
       
        buf = bytearray([0,0,0,0])
        self.i2c.mem_read(buf, 0x28, 0x14)
        xvraw, yvraw = (struct.unpack('<hh', buf))
        self.rollv = xvraw/900
        self.pitchv = yvraw/900
        
        
        pass
    
    
    
    
    
    
    
    
    
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    
    
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
   
    myI2C = I2C(1, I2C.CONTROLLER)
    offset_array = b'\xf3\xff\xfa\xff\xe5\xff\x00\x10\x10\x01\x04\x00\xff\xff\xff\xff\x00\x00\xe8\x03\xe0\x01'
    driver1 = BNO055(myI2C)
    driver1.IMU_mode()
    driver1.calib_status()
    driver1.calib_write(offset_array)
    while True:
        driver1.angular_velo()
        driver1.calib_read()
        driver1.calib_status()
        time.sleep(3)
        #print(driver1.i2c.mem_read(1, 0x28, 0x00))
    
    