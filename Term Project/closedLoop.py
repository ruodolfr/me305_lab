"""!@file closedLoop.py
    @brief Closed-loop PD controller class file.
    @details Generalized proportional-derivative (PD) controller. Takes in two
    gains (Kp and Kd), as well as positional and velocity target data and measured 
    data.
    @author Theo Philliber
    @author Ruodolf Rumbaoa
    @date 3/18/2022
"""

class ClosedLoop:
    '''!@brief ClosedLoop controller class.
        @details Takes in gains and data, and outputs the PD-controller output.
    '''
        
    def __init__(self, x_ref, xdot_ref, Kp, Kd):
        '''!@brief Constructs a Closed-Loop Controller object
            @details Sets target data values and initial gains.
            @param x_ref Initial reference data point.
            @param xdot_ref Intial reference for derivative of data point.
            @param Kp Proportional gain value.
            @param Kd Derivative gain value.
        '''
        self.Kp = Kp
        self.Kd = Kd
        pass
    
    def update(self, x_ref, x_meas, xdot_ref, xdot_meas):
        '''!@brief Calculates controller-adjusted value.
            @details Multiplies gains by error (reference data - measured data).
            @param x_ref Reference data point.
            @param x_meas Measured data pont.
            @param xdot_ref Reference point of derivative of data.
            @param xdot_meas Measured derivative of data.
            @return Returns value calculated by controller. Value meaning depends
            on inputs/use of controller.
        '''
        #Proportional Contribution
        data_adjusted = self.Kp*(x_ref - x_meas) + self.Kd*(xdot_ref - xdot_meas)
        
        #Derivative contribution
        
# =============================================================================
#         
#         if duty_adjusted > 40:
#             duty_adjusted = 40
#         elif duty_adjusted < -40:
#             duty_adjusted = -40
# =============================================================================
        return data_adjusted
    
    def set_gain(self, gain, gain_type):
        '''!@brief Change controller gain values.
            @param gain Gain value.
            @param gain_type Gain type -- 1 for proportional, 2 for derivative.
        '''
        
        
        ## Gain_type: Kp = 1, Kd = 2;
        if gain_type == 1:
            self.Kp = gain
        elif gain_type == 2:
            self.Kd = gain
        else:
            pass
    